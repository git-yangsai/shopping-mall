import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'Login',
        component: Login
    },
    {
        path: '/index',
        name: 'Index',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
            import ('../views/Index.vue')
    },
    {
        path: '/shopping-cart',
        name: 'shoppingCart',
        component: () =>
            import ('../views/shoppingCart.vue')
    },
    {
        path: '/my-info',
        name: 'myInfo',
        component: () =>
            import ('../views/myInfo.vue')
    },
    {
        path: '/my-order',
        name: 'MyOrder',
        meta: {
            title: "我的订单"
        },
        component: () =>
            import ('../views/myOrder.vue')
    },
    {
        path: '/search-result',
        name: 'SearchResult',
        meta: {
            title: "搜索结果"
        },
        component: () =>
            import ('../views/searchResult.vue')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})


const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}
export default router