const common = {
    state: {
        currentRouteName: "Index",
        cartLen: 0
    },
    mutations: {
        setCurrentRouteName(state, data) {
            state.currentRouteName = data;
        },
        setCartLen(state, data) {
            state.cartLen = data
        },
    },
    actions: {

    }
}

export default common