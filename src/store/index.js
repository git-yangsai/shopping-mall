import Vue from 'vue'
import Vuex from 'vuex'
import common from './modules/common'
Vue.use(Vuex)


const modules = {
    common
}
const Store = new Vuex.Store({
    modules
})

export default Store