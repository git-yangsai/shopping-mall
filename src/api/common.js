import base from './base' // 导入接口域名列表
import axios from '@/http' // 导入http中创建的axios实例


const common = {
    // 登录
    login(params) {
        return axios.post(`${base.login}`, params)
    }
}

export default common