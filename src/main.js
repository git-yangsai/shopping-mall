import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import '@/assets/css/common.less' /* 引入公共样式 */
import common from '@/utils/common.js'

import Vant from 'vant';
// import 'vant/lib/index.css';
import '@/assets/css/global.less'

import api from '@/api'

Vue.use(Vant);
Vue.config.productionTip = false
Vue.prototype.$api = api;
Vue.prototype.$common = common;
Vue.use(ElementUI);
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')